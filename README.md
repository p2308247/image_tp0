HERDA Abdelaziz 
12308247


# Image Tp0

# Lien vers la forge
https://forge.univ-lyon1.fr/p2308247/image_tp0.git

# Le fichier tp1.py 
Il contient le code de ce tp.

Une partie du code représente l'implémentation en cv2.
L'autre représente l'implémentation manuelle.
Chaque partie est précédée d'un commentaire lui décrivant.

A la fin, les figures qui sont affichés sont : 
- L'histogramme normalisé.
- L'histogramme étiré.
- L'histogramme égalisé.



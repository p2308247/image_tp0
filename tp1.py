import cv2
import numpy as np
import matplotlib.pyplot as plt


image = cv2.imread('/home/abdelaziz/Images/tore.png', cv2.IMREAD_GRAYSCALE)

image_array = np.array(image)


# 1- Histogramme de l'image 
hist = cv2.calcHist([image], [0], None, [256], [0, 256])


# 2- Normalisation de l'histogramme
normalized_hist = image_array / np.sum(hist)

# 3- Etirement de l'histogramme sans cv2
# Calcul du minimum et du maximum
min = np.min(image_array)
max = np.max(image_array)
newMin = 200
newMax = 255
stretched_hist = []
for i in range(len(image_array)) :
    stretched_hist.append((normalized_hist[i] - min) / (max - min) * (newMax - newMin) + newMin)

# Etirement de l'histogramme avec cv2
expanded_image = cv2.normalize(image, None, newMin, newMax, cv2.NORM_MINMAX)
stretched_hist_cv = cv2.calcHist([expanded_image], [0], None, [256], [0, 256])

# 3- Histogramme egalizé avec cv2
equ = cv2.equalizeHist(image)
# Sans cv2 
hist, bins = np.histogram(image_array.flatten(), 256, [0, 256])
cdf = hist.cumsum()
cdf_normalized = cdf * hist.max() / cdf.max()
# Appliquer l'égalisation d'histogramme
img_equalized = np.interp(image_array.flatten(), bins[:-1], cdf_normalized)
# Remettre l'image à sa forme originale
img_equalized = img_equalized.reshape(image_array.shape)


# Affichage de l'histogramme
plt.figure(1)
plt.title("Egalisation de l'image")
plt.subplot(1, 2, 1), plt.imshow(image, cmap='gray'), plt.title('Image originale')
plt.subplot(1, 2, 2), plt.imshow(equ, cmap='gray'), plt.title('Image égalisée')

plt.figure(2)
plt.title("Etirement de l'image")
plt.plot(stretched_hist_cv)

plt.figure(3)
plt.title("Image normalisée")
plt.plot(normalized_hist)

plt.show()

# Algorithme de filtrage 
def filtrage(image, noyau):
    hauteur, largeur = image.shape
    nouvelle_image = np.zeros((hauteur, largeur), dtype=np.uint8)

    for ligne in range(1, hauteur-1):
        for colonne in range(1, largeur-1):
            somme = 0
            for i in range(-1, 2):
                for j in range(-1, 2):
                    somme += image[ligne + i, colonne + j] * noyau[i + 1, j + 1]

            nouvelle_image[ligne, colonne] = np.clip(somme, 0, 255)

    return nouvelle_image